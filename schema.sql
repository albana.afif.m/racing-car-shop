-- phpMyAdmin SQL Dump 
-- version 5.2.0 
-- https://www.phpmyadmin.net/ 
-- 
-- Host: 127.0.0.1 
-- Generation Time: Oct 02, 2022 at 04:22 PM 
-- Server version: 10.4.25-MariaDB 
-- PHP Version: 8.1.10 
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO"; 
START TRANSACTION; 
SET time_zone = "+00:00"; 
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */; 
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */; 
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */; 
/*!40101 SET NAMES utf8mb4 */; 
-- 
-- Database: `car` 
-- 
-- -------------------------------------------------------- 
-- 
-- Table structure for table `car` 
-- 
CREATE TABLE `car` ( 
 `CarID` char(5) NOT NULL, 
 `NamaPembeli` varchar(100) NOT NULL, 
 `Merk` varchar(50) NOT NULL, 
 `JenisMobil` varchar(50) NOT NULL, 
 `Mesin` varchar(20) NOT NULL, 
 `JumlahPenumpang` int(11) NOT NULL, 
 `Qty` int(11) NOT NULL, 
 `TotalHarga` int(11) NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 
-- 
-- Dumping data for table `car` 
-- 
INSERT INTO `car` (`CarID`, `NamaPembeli`, `Merk`, `JenisMobil`, `Mesin`, `JumlahPenumpang`, `Qty`, `TotalHarga`) 
VALUES 
('MD001', 'Ronald', 'Mitsubishi', 'MPV', 'Diesel', 8, 2, 1000000000), 
('SH001', 'Erik', 'Toyota', 'SUV', 'Hybrid', 5, 1, 635000000); 
-- 
-- Indexes for dumped tables 
-- 
-- 
-- Indexes for table `car` 
-- 
ALTER TABLE `car` 
 ADD PRIMARY KEY (`CarID`); 
COMMIT; 
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */; 
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */; 
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
