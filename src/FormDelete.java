import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class FormDelete extends Form {

    JPanel p1 = new JPanel();
    GridLayout gridLayout = new GridLayout(1,1); // Row, Column

    JPanel p2 = new JPanel();
    FlowLayout flowLayout = new FlowLayout();

    JTextField carIDTextF = new JTextField();

    JButton delete = new JButton("Delete");
    JButton exit = new JButton("Exit");

    public FormDelete() {
        setContentForm();

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);

        this.setSize(400, 100);
        this.setVisible(true);
    }

    public void deleteButtonAction(ActionEvent e) {
        CarOrder order = new CarOrder();
        order.deleteCarOrder(carIDTextF.getText());
        JOptionPane.showMessageDialog(null, "Success");
    }

    public void exitButtonAction(ActionEvent e ) {
        this.dispose();
    }

    public void setContentForm() {
        p1.setLayout(gridLayout);

        p1.add(new JLabel("CarID"));
        p1.add(carIDTextF);

        p2.setLayout(flowLayout);

        delete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteButtonAction(e);
            }
        });
        p2.add(delete);

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exitButtonAction(e);
            }
        });
        p2.add(exit);

        this.add(p1, BorderLayout.NORTH);
        this.add(p2, BorderLayout.SOUTH);
    }

    public void actionPerformed(ActionEvent e) {}
}