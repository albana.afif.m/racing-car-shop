import java.util.List;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;

final class FormDisplay extends Form {
    private JScrollPane content = new JScrollPane();
    private JTable table = new JTable();
    private DefaultTableModel dtm = new DefaultTableModel();

    private JPanel menu = new JPanel();
    private FlowLayout flowLayout = new FlowLayout();

    JButton refresh = new JButton("Refresh");
    JButton insert = new JButton("Insert");
    JButton update = new JButton("Update");
    JButton delete = new JButton("Delete");

    public FormDisplay() {
        setMenuForm();
        setContentForm();

        this.setSize(950, 400);
        this.add(menu, BorderLayout.NORTH);
        this.add(content, BorderLayout.CENTER);
        this.setVisible(true);
    }

    private void refreshButtonAction(ActionEvent e) {
        unsetContentForm();
        setContentForm();
    }

    private void insertButtonAction(ActionEvent e) {
        new FormInsert();
    }

    private void updateButtonAction(ActionEvent e) {
        new FormUpdate();
    }

    private void deleteButtonAction(ActionEvent e) {
        new FormDelete();
    }

    private void setMenuForm() {
        menu.setLayout(flowLayout);

        menu.add(new JLabel("Menu: "));

        refresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                refreshButtonAction(e);
            }
        });
        menu.add(refresh);

        insert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                insertButtonAction(e);
            }
        });
        menu.add(insert);

        update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateButtonAction(e);
            }
        });
        menu.add(update);

        delete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteButtonAction(e);
            }
        });
        menu.add(delete);
    }

    protected void setContentForm() {
        table.setModel(dtm);
        content.setViewportView(table);
        refreshData();
    }

    private void unsetContentForm() {
        dtm.setColumnCount(0);
        dtm.getDataVector().removeAllElements();
        dtm.fireTableStructureChanged();
        dtm.fireTableDataChanged();
    }

    private void refreshData() {
        dtm.addColumn("CarID");
        dtm.addColumn("NamaPembeli");
        dtm.addColumn("Merk");
        dtm.addColumn("JenisMobil");
        dtm.addColumn("Mesin");
        dtm.addColumn("JumlahPenumpang");
        dtm.addColumn("Qty");
        dtm.addColumn("TotalHarga");

        CarOrder carOrder = new CarOrder();
        List<OrderItem> orders = carOrder.listCarOrder();

        for (OrderItem order : orders) {
            dtm.addRow(new Object[] {
                order.getCarID(),
                order.customer.getName(),
                order.car.brand.getName(),
                order.car.model.getName(),
                order.car.engine.getName(),
                order.car.model.getPax(),
                order.getQty(),
                order.getTotalPrice()
            });
        }
    }

    public void actionPerformed(ActionEvent e) {};
}