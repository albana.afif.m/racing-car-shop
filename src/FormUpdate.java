import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class FormUpdate extends Form {
    JPanel p1 = new JPanel();
    JPanel p2 = new JPanel();
    JPanel p3 = new JPanel();
    JPanel p4 = new JPanel();
    JPanel panel = new JPanel();

    JTextField carIDTextF = new JTextField();
    JTextField custName;
    JTextField qty;
    JTextField totalPrice;

    JButton check = new JButton("Check");
    JButton update = new JButton("Update");
    JButton exit = new JButton("Exit");

    OrderItem io;

    public FormUpdate() {
        setCheckForm();

        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        this.add(panel, BorderLayout.NORTH);
        this.setSize(400, 400);
        this.setVisible(true);
    }

    public void checkButtonAction(ActionEvent e) {
        System.out.println("Trigger check button");

        CarOrder order = new CarOrder();
        this.io = order.findCarOrderByID(carIDTextF.getText());

        setContentForm();
        this.revalidate();
     }

    public void updateButtonAction(ActionEvent e) {
        System.out.println("Trigger update button");

        CarOrder order = new CarOrder();

        if (custName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(
                null, "NamaPembeli is empty", "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else if (qty.getText().isEmpty())  {
            JOptionPane.showMessageDialog(
                null, "Qty is empty", "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else if (pax.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(
                null, "JumlahPenumpang is empty. Please choose JenisMobil then JumlahPenumpang again.",
                    "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else if (Integer.parseInt(qty.getText()) < 1 || 
                    Integer.parseInt(qty.getText()) > 10) {
            JOptionPane.showMessageDialog(
                null, "Qty must between 1 to 10", "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else {
            order.setCarID(carIDTextF.getText());
            order.customer.setName(custName.getText());
            order.car.brand.setName(brand.getSelectedItem().toString());
            order.car.model.setName(model.getSelectedItem().toString());
            order.car.engine.setName(engine.getSelectedItem().toString());
            order.car.model.setPax(Integer.parseInt(pax.getSelectedItem().toString()));
            order.setQty(Integer.parseInt(qty.getText()));
            order.setTotalPrice(Integer.parseInt(totalPrice.getText()));

            order.updateCarOrder();
            JOptionPane.showMessageDialog(null, "Success");
        }
    }

    public void setCheckForm() {
        p1.setLayout(new GridLayout(1, 2));

        p1.add(new JLabel("CarID"));
        p1.add(carIDTextF);

        check.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkButtonAction(e);
            }
        });
        p2.add(check);

        panel.add(p1);
        panel.add(p2);
    }

    @Override
    public void setContentForm() {
        p3.setLayout(new GridLayout(8,2)); //Row, Column

        p3.add(new JLabel("NamaPembeli"));
        p3.add(custName = new JTextField(io.customer.getName()));

        p3.add(new JLabel("Merk"));
        brand.setSelectedItem(io.car.brand.getName());
        p3.add(brand);

        p3.add(new JLabel("JenisMobil"));

        model.setSelectedItem(io.car.model.getName());
        model.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setPaxItem(e);
            }
        });
        model.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
        p3.add(model);

        p3.add(new JLabel("Mesin"));
        engine.setSelectedItem(io.car.engine.getName());
        p3.add(engine);

        p3.add(new JLabel("JumlahPenumpang"));

        paxItems.put(models[0], paxSUV);
        paxItems.put(models[1], paxMPV);
        paxItems.put(models[2], paxHatchback);
        p3.add(pax);

        p3.add(new JLabel("Qty"));
        p3.add(qty = new JTextField(Integer.toString(io.getQty())));

        p3.add(new JLabel("TotalPrice"));
        p3.add(totalPrice = new JTextField(Integer.toString(io.getTotalPrice())));

        update.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                updateButtonAction(e);
            }
        });
        p4.add(update);

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exitButtonAction(e);
            }
        });
        p4.add(exit);

        panel.add(p3);
        panel.add(p4);
    }

    public void actionPerformed(ActionEvent e) {}
}