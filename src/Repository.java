import java.util.*;
import java.sql.*;

interface CarOrderRepository {
    String findLastCarID(String letterCode);
    OrderItem findCarOrderByID(String carID);
    List<OrderItem> selectAllCarOrder();

    void saveCarOrder(OrderItem order);
    void updateCarOrder(OrderItem order);
    void deleteCarOrder(String carID);
}

class DatabaseCarOrderStore implements CarOrderRepository {

    public String findLastCarID(String letterCode) {
        String query = "SELECT CarID FROM car WHERE CarID LIKE '"
                        + letterCode
                        + "%' ORDER BY CarID DESC LIMIT 1";
        
        Database db = new Database();
        try {
            ResultSet result = db.executeQuery(query);
            result.next();
            var carID = result.getString("CarID");
            return carID;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
        return "";
    }

    public OrderItem findCarOrderByID(String carID) {
        String query = "SELECT * FROM car WHERE CarID=?";

        Database db = new Database();
        try {
            ResultSet result = db.executeQuery(query, carID);
            result.next();

            Customer customer = new Customer();
            customer.setName(result.getString("NamaPembeli"));

            Car car = new Car();
            car.brand.setName(result.getString("Merk"));
            car.model.setName(result.getString("JenisMobil"));
            car.engine.setName(result.getString("Mesin"));
            car.model.setPax(result.getInt("JumlahPenumpang"));

            OrderItem order = new OrderItem(customer, car);
            order.setCarID(result.getString("CarID"));
            order.setQty(result.getInt("Qty"));
            order.setTotalPrice(result.getInt("TotalHarga"));
            
            return order;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
        return null;
    }

    public List<OrderItem> selectAllCarOrder() {
        String query = "SELECT * FROM car ORDER BY CarID";
        List<OrderItem> orderList = new ArrayList<>();

        Database db = new Database();
        try {
            ResultSet result = db.executeQuery(query);

            while( result.next() ) {

                Customer customer = new Customer();
                customer.setName(result.getString("NamaPembeli"));

                Car car = new Car();
                car.brand.setName(result.getString("Merk"));
                car.model.setName(result.getString("JenisMobil"));
                car.engine.setName(result.getString("Mesin"));
                car.model.setPax(result.getInt("JumlahPenumpang"));

                OrderItem order = new OrderItem(customer, car);
                order.setCarID(result.getString("CarID"));
                order.setQty(result.getInt("Qty"));
                order.setTotalPrice(result.getInt("TotalHarga"));

                orderList.add(order);
            }
            return orderList;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
        return List.of();
    }

    public void saveCarOrder(OrderItem order) {
        String query = "INSERT INTO car "
        + " (CarID, NamaPembeli, Merk, JenisMobil, Mesin, JumlahPenumpang, Qty, TotalHarga)"
        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        Database db = new Database();
        try {
            db.executeUpdate(query, 
                order.getCarID(),
                order.customer.getName(),
                order.car.brand.getName(),
                order.car.model.getName(),
                order.car.engine.getName(),
                order.car.model.getPax(),
                order.getQty(),
                order.getTotalPrice()
            );
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
    }

    public void updateCarOrder(OrderItem order) {
        String query = "UPDATE car SET" 
        + " NamaPembeli=?, Merk=?, JenisMobil=?, Mesin=?, JumlahPenumpang=?, Qty=?, TotalHarga=?" 
        + " WHERE CarID=?";

        Database db = new Database();
        try {
            db.executeUpdate(query, 
                order.customer.getName(),
                order.car.brand.getName(),
                order.car.model.getName(),
                order.car.engine.getName(),
                order.car.model.getPax(),
                order.getQty(),
                order.getTotalPrice(),
                order.getCarID()
            );
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
    }

    public void deleteCarOrder(String carID) {
        String query = "DELETE FROM car WHERE CarID = ?";
        Database db = new Database();
        try {
            db.executeUpdate(query, carID);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            db.releaseConnection();
        }
    }
}