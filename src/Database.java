import java.sql.*;

class Database {
    private String url = "jdbc:mysql://127.0.0.1/car";
    private String username = "root";
    private String password = "password";

    public Connection connection;

    public Database() {
        try {
            this.connection = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public ResultSet executeQuery(String query) {
        try {
            if (connection != null) {
                PreparedStatement stmt = this.connection.prepareStatement(query);
                ResultSet rs = stmt.executeQuery();
                return rs;
            }
            throw new Exception("execQuery() has no database connection");
        } catch (Exception e) {
            System.out.println(e);
        } 
        return null;
    }

    public ResultSet executeQuery(String query, Object ...args) {
        try {
            if (connection != null) {
                PreparedStatement stmt = this.connection.prepareStatement(query);

                int i = 1;
                for (Object arg : args) {
                    stmt.setObject(i, arg);
                    i++;
                }
                ResultSet rs = stmt.executeQuery();
                return rs;
            }
            throw new Exception("execQuery() has no database connection");
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public void executeUpdate(String query, Object ...args) {
        try {
            if (connection != null) {
                PreparedStatement stmt = this.connection.prepareStatement(query);

                int i = 1;
                for (Object arg : args) {
                    System.out.println(arg);
                    stmt.setObject(i, arg);
                    i++;
                }
                stmt.executeUpdate();
                return;
            }
            throw new Exception("executeUpdate() has no database connection");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void releaseConnection() {
        try {
            this.connection.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    } 
}