import java.util.*;

public class App {
    public static void main(String[] args) {

        new FormDisplay();
    }
}

class CarOrder extends OrderItem {

    public CarOrder() {
        super(new Customer(), new Car());
    }

    public void createNewCarOrder() {
        calculateTotalPrice();
        generateCarID();

        repository.saveCarOrder(this);
    }

    public void updateCarOrder() {
        calculateTotalPrice();
        repository.updateCarOrder(this);
    }

    public void deleteCarOrder(String carID) {
        repository.deleteCarOrder(carID);
    }

    public OrderItem findCarOrderByID(String carID) {
        return repository.findCarOrderByID(carID);
    }

    public List<OrderItem> listCarOrder() {
        return repository.selectAllCarOrder();
    }
}

class OrderItem {
    private String carID;
    private int qty;
    private int totalPrice;

    protected Customer customer;
    protected Car car;

    protected DatabaseCarOrderStore repository = new DatabaseCarOrderStore();

    public OrderItem(Customer customer, Car car) {
        this.customer = customer;
        this.car = car;
    }

    public void setCarID(String carID) {
        this.carID = carID;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setTotalPrice() {
        calculateTotalPrice();
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCarID() {
        return this.carID;
    }

    public int getQty() {
        return this.qty;
    }

    public int getTotalPrice() {
        return this.totalPrice;
    }

    protected void calculateTotalPrice() {
        this.totalPrice = qty * (car.brand.getPrice() + car.model.getPrice() + car.engine.getPrice());
    }

    protected void generateCarID() {
        String modelFirstLetter = car.model.getName().substring(0, 1).toUpperCase();
        String engineFirstLetter = car.engine.getName().substring(0, 1).toUpperCase();
        String letterCode = modelFirstLetter + engineFirstLetter;
        String lastCarID = repository.findLastCarID(letterCode);

        if (lastCarID.isEmpty()) {
            this.carID = letterCode + String.format("%03d", 1);
        } else {
            String carIDNumber = lastCarID.substring(2, 5);
            int tempCarIDNumber = Integer.parseInt(carIDNumber);
            this.carID = letterCode + String.format("%03d", tempCarIDNumber + 1);
        }
    }
}

interface Vehicle {
    void brand(String brand);
    void model(String model, int pax);
    void engine(String engine);
}

class Car implements Vehicle {
    Brand brand = new Brand();
    Model model = new Model();
    Engine engine = new Engine();

    public void brand(String brand) {
        this.brand.setName(brand);
    }

    public void model(String model, int pax) {
        this.model.setName(model);
        this.model.setPax(pax);
    }

    public void engine(String engine) {
        this.engine.setName(engine);
    }
}

class Brand {
    private String name;
    private int price;

    public void setName(String name) {
        this.name = name;
        this.price = setBrandPrice();
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    private int setBrandPrice() {
        switch(this.name.toLowerCase()) {
        case "toyota":
            return 15000000;
        case "honda":
            return 20000000;
        case "mitsubishi":
            return 10000000;
        default:
            return 0;
        }
    }
}

class Model {
    private String name;
    private int price;
    private int pax;

    public void setName(String name) {
        this.name = name;
        this.price = setModelPrice();
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setPax(int pax) {
        this.pax = pax;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    public int getPax() {
        return this.pax;
    }

    private int setModelPrice() {
        switch(this.name.toLowerCase()) {
        case "suv":
            return 500000000;
        case "mpv":
            return 400000000;
        case "hatchback":
            return 450000000;
        default:
            return 0;
        }
    }
}

class Engine {
    private String name;
    private int price;

    public void setName(String name) {
        this.name = name;
        this.price = setEnginePrice();
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return this.name;
    }

    public int getPrice() {
        return this.price;
    }

    private int setEnginePrice() {
        switch(this.name.toLowerCase()) {
        case "bensin":
            return 60000000;
        case "diesel":
            return 90000000;
        case "hybrid":
            return 120000000;
        case "listrik":
            return 200000000;
        default:
            return 0;
        }
    }
}

class Customer {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
