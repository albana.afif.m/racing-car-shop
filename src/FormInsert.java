import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

class FormInsert extends Form {
    JPanel p1 = new JPanel();
    GridLayout gridLayout = new GridLayout(8,2); // Row, Column

    JPanel p2 = new JPanel();
    FlowLayout flowLayout = new FlowLayout();

    JTextField custName = new JTextField();
    JTextField qty = new JTextField();

    JButton save = new JButton("Submit");
    JButton exit = new JButton("Exit");

    public FormInsert() {
        setContentForm();

        this.setSize(400, 270);
        this.setVisible(true);
    }
    
    private void saveButtonAction(ActionEvent e) {
        CarOrder order = new CarOrder();

        if (custName.getText().isEmpty()) {
            JOptionPane.showMessageDialog(
                null, "NamaPembeli is empty", "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else if (qty.getText().isEmpty())  {
            JOptionPane.showMessageDialog(
                null, "Qty is empty", "Warning" , JOptionPane.WARNING_MESSAGE);
        } 
        else if (pax.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(
                null, "JumlahPenumpang is empty. Please choose JenisMobil then JumlahPenumpang again.",
                    "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else if (Integer.parseInt(qty.getText()) < 1 || 
                    Integer.parseInt(qty.getText()) > 10) {
            JOptionPane.showMessageDialog(
                null, "Qty must between 1 to 10", "Warning" , JOptionPane.WARNING_MESSAGE);
        }
        else {
            order.customer.setName(custName.getText());
            order.car.brand.setName(brand.getSelectedItem().toString());
            order.car.model.setName(model.getSelectedItem().toString());
            order.car.engine.setName(engine.getSelectedItem().toString());
            order.car.model.setPax(Integer.parseInt(pax.getSelectedItem().toString()));
            order.setQty(Integer.parseInt(qty.getText()));
    
            order.createNewCarOrder();
            JOptionPane.showMessageDialog(null, "Success");
        }
    }

    public void setContentForm() {
        p1.setLayout(gridLayout);

        p1.add(new JLabel("NamaPembeli"));
        p1.add(custName);

        p1.add(new JLabel("Merk"));
        p1.add(brand);

        p1.add(new JLabel("JenisMobil"));

        model.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setPaxItem(e);
            }
        });
        model.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
        p1.add(model);

        p1.add(new JLabel("Mesin"));
        p1.add(engine);

        p1.add(new JLabel("JumlahPenumpang"));

        paxItems.put(models[0], paxSUV);
        paxItems.put(models[1], paxMPV);
        paxItems.put(models[2], paxHatchback);
        p1.add(pax);

        p1.add(new JLabel("Qty"));
        p1.add(qty);

        p2.setLayout(flowLayout);

        save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveButtonAction(e);
            }
        });
        p2.add(save);

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exitButtonAction(e);
            }
        });
        p2.add(exit);

        this.add(p1, BorderLayout.NORTH);
        this.add(p2, BorderLayout.SOUTH);
    }

    public void actionPerformed(ActionEvent e) {}
}