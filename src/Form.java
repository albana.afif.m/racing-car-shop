import java.awt.event.*;
import javax.swing.*;
import java.util.Hashtable;

abstract class Form extends JFrame implements ActionListener {
    String[] brands = { "Toyota", "Honda", "Mitsubishi"};
    String[] models = { "SUV", "MPV", "Hatchback"};
    String[] engines = { "Bensin", "Diesel", "Hybrid", "Listrik"};

    String[] paxSUV = { "5", "7" };
    String[] paxMPV = { "7", "8", "10" };
    String[] paxHatchback = { "5" };

    JComboBox<String> brand = new JComboBox<>(brands);
    JComboBox<String> model = new JComboBox<>(models);
    JComboBox<String> engine = new JComboBox<>(engines);
    JComboBox<String> pax = new JComboBox<>();

    Hashtable<String, String[]> paxItems = new Hashtable<>();

    public Form() {
        this.setTitle("Racing Car Shop");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationByPlatform(true);
    }

    protected void setPaxItem(ActionEvent e) {
        String item = (String)model.getSelectedItem();
        Object obj = paxItems.get(item);

        if (obj == null) {
            pax.setModel(new DefaultComboBoxModel<>());
        } 
        else {
            pax.setModel(new DefaultComboBoxModel<>((String[])obj));
        }
    }

    protected void exitButtonAction(ActionEvent e) {
        this.dispose();
    }

    abstract void setContentForm(); 
}